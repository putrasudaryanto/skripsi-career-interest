<?php
/**
 * AssessmentModule
 * version: 0.0.1
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2017 Ommu Platform (opensource.ommu.co)
 * @created date 13 November 2017, 09:53 WIB
 * @link https://github.com/ommu/ommu-assessment
 *
 *----------------------------------------------------------------------------------------------------------
 */

Yii::import('application.vendor.ommu.assessment.AssessmentModule');