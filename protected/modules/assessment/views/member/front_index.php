<?php
/**
 * Assessment Career Tests (assessment-career-tests)
 * @var $this MemberController
 * @var $model AssessmentCareerTests
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 31 July 2018, 19:27 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 */

	$this->breadcrumbs=array(
		'Assessments'=>Yii::app()->controller->createUrl('member/index'),
	);

	$cs = Yii::app()->getClientScript();
$js=<<<EOP
	var cdate = new Date($("#countDownDate").val()).getTime();
	function countDown() {
		var now = new Date().getTime();
		var distance = cdate - now;
		if(distance < 0) {
			clearInterval(cdown);
			document.getElementById("timer").innerHTML = ($("#countDownDate").attr("expireword"));
		} else {
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor(distance % (1000 * 60 * 60 *24) / (1000 * 60 * 60));
			var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
			var seconds = Math.floor(distance % (1000 * 60) / 1000);
			if(days < 10){days = "0" + days;}
			if(hours < 10){hours = "0" + hours;}
			if(minutes < 10){minutes = "0" + minutes;}
			if(seconds < 10) {seconds = "0" + seconds;}
			document.getElementById("timer").innerHTML = days + "<span>d</span> :" + hours + "<span>h</span> :" + minutes + "<span>m</span> <span>" + seconds + "</span>";
		}
	}
	countDown();
	var cdown = setInterval(function(){
		countDown();
	}, 1000);
EOP;
	$cs->registerScript('dialog-alert', $js, CClientScript::POS_END);
?>

<div class="card assessment">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-12">
			<div class="body align-center m-t-20">
				<img src="<?php echo Yii::app()->request->baseUrl;?>/public/redactor/minat_karir.png">
				<?php if($interestTest['waiting']) {?>
				<input type="hidden" id="countDownDate" name="countDownDate" value="<?php echo $interestTest['next_test_date'];?>" expireword="Welcome">
				<h4 class="timer m-t-20"><code id="timer"></code></h4>
				<?php }?>
			</div>
		</div>
		<div class="col-lg-9 col-md-8 col-sm-12">
			<div class="body">
				<?php echo $this->parseTemplate($setting->message_alert['intro']);?>
				<hr/>
				<?php if($interestTest['waiting']) {
					echo $this->parseTemplate($setting->message_alert['description']['running'], array(
						'period_test_difference'=>$setting->period_test_difference,
						'period_test_diff_type'=>AssessmentCareerSetting::getPeriodTestDiffType($setting->period_test_diff_type),
						'start_date'=>$this->dateFormat($interestTest['start_date']),
						'next_test_date'=>$this->dateFormat($interestTest['next_test_date']),
					));?>
					<a class="btn btn-raised btn-primary waves-effect" href="<?php echo Yii::app()->createUrl('assessment/careerTest/result', array('id'=>$interestTest['id']));?>" title="<?php echo Yii::t('phrase', 'Lihat Hasil');?>"><?php echo Yii::t('phrase', 'Lihat Hasil');?></a>

				<?php } else {
					$buttonLabel = $interestTest['running'] ? Yii::t('phrase', 'Lanjutkan Tes') : Yii::t('phrase', 'Mulai Tes');
					echo $this->parseTemplate($setting->message_alert['description']['new'], array(
						'period_test_difference'=>$setting->period_test_difference,
						'period_test_diff_type'=>AssessmentCareerSetting::getPeriodTestDiffType($setting->period_test_diff_type),
					));?>
					<a class="btn btn-raised btn-primary waves-effect" href="<?php echo Yii::app()->createUrl('assessment/careerTest/index');?>" title="<?php echo $buttonLabel;?>"><?php echo $buttonLabel;?></a>
				<?php }?>
			</div>
		</div>
	</div>
</div>
