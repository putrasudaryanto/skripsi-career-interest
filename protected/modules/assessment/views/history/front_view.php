<?php
/**
 * Assessment Career Tests (assessment-career-tests)
 * @var $this HistoryController
 * @var $model AssessmentCareerTests
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 29 July 2018, 18:24 WIB
 * @modified date 29 July 2018, 18:24 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 */

	$this->breadcrumbs=array(
		'Assessment Career Tests'=>array('manage'),
		$model->user->displayname,
	);
?>

<?php //begin.Messages ?>
<div id="ajax-message">
<?php if(Yii::app()->user->hasFlash('success'))
	echo $this->flashMessage(Yii::app()->user->getFlash('success'), 'success');?>
</div>
<?php //end.Messages ?>

<div class="box">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
			'name'=>'test_id',
			'value'=>$model->test_id,
		),
		array(
			'name'=>'publish',
			'value'=>$model->publish ? Yii::t('phrase', 'Publish') : Yii::t('phrase', 'Unpublish'),
			'type'=>'raw',
		),
		array(
			'name'=>'user_search',
			'value'=>$model->user->displayname ? $model->user->displayname : '-',
		),
		array(
			'name'=>'start_date',
			'value'=>!in_array($model->start_date, array('0000-00-00 00:00:00','1970-01-01 00:00:00','0002-12-02 07:07:12','-0001-11-30 00:00:00')) ? $this->dateFormat($model->start_date) : '-',
		),
		array(
			'name'=>'finish_date',
			'value'=>!in_array($model->finish_date, array('0000-00-00 00:00:00','1970-01-01 00:00:00','0002-12-02 07:07:12','-0001-11-30 00:00:00')) ? $this->dateFormat($model->finish_date) : '-',
		),
		array(
			'name'=>'next_test_date',
			'value'=>!in_array($model->next_test_date, array('0000-00-00 00:00:00','1970-01-01 00:00:00','0002-12-02 07:07:12','-0001-11-30 00:00:00')) ? $this->dateFormat($model->next_test_date) : '-',
		),
		array(
			'name'=>'career_interest',
			'value'=>$model->career_interest ? $model->career_interest : '-',
		),
		array(
			'name'=>'evaluation_grade',
			'value'=>$model->evaluation_grade ? $model->evaluation_grade : '-',
		),
		array(
			'name'=>'evaluation_date',
			'value'=>!in_array($model->evaluation_date, array('0000-00-00 00:00:00','1970-01-01 00:00:00','0002-12-02 07:07:12','-0001-11-30 00:00:00')) ? $this->dateFormat($model->evaluation_date) : '-',
		),
		array(
			'name'=>'modified_date',
			'value'=>!in_array($model->modified_date, array('0000-00-00 00:00:00','1970-01-01 00:00:00','0002-12-02 07:07:12','-0001-11-30 00:00:00')) ? $this->dateFormat($model->modified_date) : '-',
		),
		array(
			'name'=>'modified_search',
			'value'=>$model->modified->displayname ? $model->modified->displayname : '-',
		),
		array(
			'name'=>'updated_date',
			'value'=>!in_array($model->updated_date, array('0000-00-00 00:00:00','1970-01-01 00:00:00','0002-12-02 07:07:12','-0001-11-30 00:00:00')) ? $this->dateFormat($model->updated_date) : '-',
		),
	),
)); ?>
</div>
