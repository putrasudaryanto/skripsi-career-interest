<?php
/**
 * Assessment Career Tests (assessment-career-tests)
 * @var $this TestController
 * @var $model AssessmentCareerTests
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 29 July 2018, 18:24 WIB
 * @modified date 29 July 2018, 18:24 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 */

	$this->breadcrumbs=array(
		'Assessments'=>Yii::app()->controller->createUrl('member/index'),
		'Career'=>Yii::app()->controller->createUrl('index'),
	);

	$message_alert_alert_new = $this->parseTemplate($setting->message_alert['alert']['new'], array(
		'period_test_difference'=>$setting->period_test_difference,
		'period_test_diff_type'=>AssessmentCareerSetting::getPeriodTestDiffType($setting->period_test_diff_type),
	));
	$message_alert_alert_new = preg_replace("/[\\n\\r]+/", "", $message_alert_alert_new);
	$agreement_url = Yii::app()->controller->createUrl('agree');

	$message_alert_alert_running = $this->parseTemplate($setting->message_alert['alert']['running'], array(
		'period_test_difference'=>$setting->period_test_difference,
		'period_test_diff_type'=>AssessmentCareerSetting::getPeriodTestDiffType($setting->period_test_diff_type),
	));
	$message_alert_alert_running = preg_replace("/[\\n\\r]+/", "", $message_alert_alert_running);
	$quiz_url = Yii::app()->controller->createUrl('quiz');

	$testCondition = $testCondition == true ? 1 : 0;

	$cs = Yii::app()->getClientScript();
	$cs->registerCssFile(Yii::app()->theme->baseUrl.'/plugins/sweetalert/sweetalert.css');
	$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/plugins/sweetalert/sweetalert.min.js', CClientScript::POS_END);
$js=<<<EOP
	var testCondition = $testCondition;
	$(function () {
		$('button#js-sweetalert').on('click', function () {
			var type = $(this).data('type');
			if (type === 'to-alert') {
				showAlertMessage();
			}
		});
	});
	function showAlertMessage() {
		if(testCondition == 0) {
			swal({
				title: 'Are you sure?',
				text: '$message_alert_alert_new',
				showCancelButton: true,
				confirmButtonText: 'Lanjut',
				closeOnConfirm: false,
				html: true
			}, function () {
				location.href = '$agreement_url';
			});
		} else {
			swal({
				title: 'Are you sure???',
				text: '$message_alert_alert_running',
				showCancelButton: true,
				confirmButtonText: 'Lanjut',
				closeOnConfirm: false,
				html: true
			}, function () {
				location.href = '$quiz_url';
			});
		}
	}
EOP;
	$cs->registerScript('dialog-alert', $js, CClientScript::POS_END);
?>

<div class="card">
	<?php echo $this->parseTemplate($setting->message_alert['attention']);?>

	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-12">
		</div>
		<div class="col-lg-9 col-md-8 col-sm-12">
			<div class="body">
				<button id="js-sweetalert" class="btn btn-raised btn-primary waves-effect" data-type="to-alert">Lanjut</button>
				<a class="btn btn-raised bg-blue-grey waves-effect" href="<?php echo Yii::app()->controller->createUrl('member/index');?>" title="Batal">Batal</a>
			</div>
		</div>
	</div>
</div>