<?php
/**
 * Assessment Career Tests (assessment-career-tests)
 * @var $this TestController
 * @var $model AssessmentCareerTests
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 29 July 2018, 18:24 WIB
 * @modified date 31 July 2018, 09:19 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 */

	$this->breadcrumbs=array(
		'Assessments'=>Yii::app()->controller->createUrl('member/index'),
		'Career'=>Yii::app()->controller->createUrl('index'),
	);

	$result = array(
		array(
			'label'=>$model->getAttributeLabel('result.score_executor'),
			'value'=>AssessmentCareerTestResult::getIndex($model->result->score_executor, true),
		),
		array(
			'label'=>$model->getAttributeLabel('result.score_thinker'),
			'value'=>AssessmentCareerTestResult::getIndex($model->result->score_thinker, true),
		),
		array(
			'label'=>$model->getAttributeLabel('result.score_creator'),
			'value'=>AssessmentCareerTestResult::getIndex($model->result->score_creator, true),
		),
		array(
			'label'=>$model->getAttributeLabel('result.score_savior'),
			'value'=>AssessmentCareerTestResult::getIndex($model->result->score_savior, true),
		),
		array(
			'label'=>$model->getAttributeLabel('result.score_persuader'),
			'value'=>AssessmentCareerTestResult::getIndex($model->result->score_persuader, true),
		),
		array(
			'label'=>$model->getAttributeLabel('result.score_organizer'),
			'value'=>AssessmentCareerTestResult::getIndex($model->result->score_organizer, true),
		)
	);
	$resultValue = CJSON::encode($result);
	$interestCount = count($interest);

	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/sparkline.bundle.js', CClientScript::POS_END); 
	$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/morrisscripts.bundle.js', CClientScript::POS_END);
$js=<<<EOP
	Morris.Donut({
		element: 'donut_chart',
		data: $resultValue,
		colors: ['rgb(0,189,209)', 'rgb(137,197,75)', 'rgb(27,138,207)', 'rgb(168,104,224)', 'rgb(255,200,0)', 'rgb(121,85,72)'],
		formatter: function (y) {
			return y
		}
	});
EOP;
	$cs->registerScript('dialog-alert', $js, CClientScript::POS_END);
?>

<div class="row clearfix">
	<div class="col-sm-12 col-md-6 col-lg-4">
		<div class="card">
			<div class="header">
				<h2><?php echo Yii::t('phrase', 'Grafik Minat');?></h2>
			</div>
			<div class="body">
				<div id="donut_chart" class="graph dashboard-donut-chart align-center"></div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-8">
		<div class="card">
			<div class="header">
				<h2><?php echo Yii::t('phrase', 'Diskripsi Minat');?></h2>
				<small><?php echo $interestCount>3 ? Yii::t('phrase', 'Kamu memiliki lebih dari 3 (tiga) minat dengan kategori tinggi') : ($interestCount==0 ? Yii::t('phrase', 'Kamu tidak memiliki minat dengan kategori tinggi') : Yii::t('phrase', 'Kamu memiliki '.$interestCount.' minat dengan kategori tinggi'));?></small>
			</div>
			<div class="body">
				<h3 class="align-center m-t-0 m-b-30"><code>"<?php echo $interestCount==0 ? Yii::t('phrase', 'Kamu Tidak Memiliki Minat') : ($interestCount==3 ? Yii::t('phrase', 'Kamu Memiliki Minat Spesifik') : Yii::t('phrase', 'Kamu Belum Memiliki Minat Spesifik'));?>"</code></h3>
				<?php echo $interestCount==0 ? Yii::t('phrase', 'Mohon maaf, hasil tes minat menunjukkan bahwa Saudara <strong>{displayname}</strong> belum memiliki peminatan yang spesifik.', array('{displayname}'=>$model->user->displayname)) : ($interestCount==3 ? Yii::t('phrase', '<p>Hasil tes minat menyebutkan bahwa Saudara <strong>{displayname}</strong> memiliki minat yang tinggi pada tipe <strong>{interest-type}</strong>. Urutan pertama menunjukkan adanya ketertarikan yang sangat tinggi dalam diri Saudara, diikuti oleh minat yang kedua dan minat yang ketiga.</p><p>Minat tersebut dapat diuraikan sebagai berikut:</p>', array('{displayname}'=>$model->user->displayname, '{interest-type}'=>implode(', ', AssessmentCareerTests::getInterestVariable($interest)))) : Yii::t('phrase', '<p>Hasil tes minat menyebutkan bahwa Saudara <strong>{displayname}</strong> memiliki minat yang tinggi pada tipe <strong>{interest-type}</strong>. Urutan pertama menunjukkan adanya ketertarikan yang sangat tinggi dalam diri Saudara, diikuti oleh minat-minat lainnya.</p><p>Minat tersebut dapat diuraikan sebagai berikut:</p>', array('{displayname}'=>$model->user->displayname, '{interest-type}'=>implode(', ', AssessmentCareerTests::getInterestVariable($interest)))));?>
			</div>
		</div>
	</div>
</div>

<?php if($interestCount!=0) {
	$criteria=new CDbCriteria;
	$criteria->select = 'variable_id, variable_name, interpretasi_desc';
	$criteria->compare('publish', 1);
	$variable = AssessmentCareerVariable::model()->findAll($criteria);?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="card">
			<div class="body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<?php 
					$i = 0;
					foreach($variable as $val) {
						if(!in_array($val->variable_id, $interest))
							continue;
						else
							$i++;?>
						<li class="nav-item"><a class="nav-link text-uppercase <?php echo $i == 1 ? 'active' : '';?>" data-toggle="tab" href="#<?php echo $this->urlTitle($val->title->message);?>" aria-expanded="true"><?php echo Yii::t('phrase', $val->title->message);?></a></li>
					<?php }?>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<?php 
					$i = 0;
					foreach($variable as $val) {
						if(!in_array($val->variable_id, $interest))
							continue;
						else
							$i++;?>
						<div role="tabpanel" class="tab-pane <?php echo $i == 1 ? 'in active' : '';?>" id="<?php echo $this->urlTitle($val->title->message);?>" aria-expanded="true">
							<?php echo $this->parseTemplate($val->interpretasi_desc, array(
								'displayname'=>$model->user->displayname,
							));?>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php }?>