<?php
/**
 * Assessment Career Tests (assessment-career-tests)
 * @var $this TestController
 * @var $model AssessmentCareerTests
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 29 July 2018, 18:24 WIB
 * @modified date 29 July 2018, 18:24 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 */

	$this->breadcrumbs=array(
		'Assessments'=>Yii::app()->controller->createUrl('member/index'),
		'Career'=>Yii::app()->controller->createUrl('index'),
	);
?>

<?php echo $this->parseTemplate($setting->message_alert['agreement'], array(
	'start_url'=>Yii::app()->controller->createUrl('quiz'),
	'cancel_url'=>Yii::app()->createUrl('site/index'),
));?>