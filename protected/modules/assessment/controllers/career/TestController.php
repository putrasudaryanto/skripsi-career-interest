<?php
/**
 * TestController
 * @var $this TestController
 * @var $model AssessmentCareerTests
 * @var $form CActiveForm
 *
 * Reference start
 * TOC :
 *	Index
 *	Agree
 *	Quiz
 *	Result
 *
 *	LoadModel
 *	performAjaxValidation
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 29 July 2018, 18:24 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 *----------------------------------------------------------------------------------------------------------
 */

class TestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';
	public $defaultAction = 'index';

	/**
	 * Initialize admin page theme
	 */
	public function init() 
	{
		$arrThemes = $this->currentTemplate('public');
		Yii::app()->theme = $arrThemes['folder'];
		$this->layout = $arrThemes['layout'];
		$this->applyViewPath(__dir__); 
	}

	/**
	 * @return array action filters
	 */
	public function filters() 
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() 
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','agree','quiz','result'),
				'users'=>array('@'),
				'expression'=>'Yii::app()->user->level',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() 
	{
		$user_id = Yii::app()->user->id;
		$setting = AssessmentCareerSetting::model()->findByPk(1);
		$userTest = AssessmentCareerUsers::model()->findByAttributes(array(
			'user_id'=>$user_id,
		));
		$testID = $userTest ? $userTest->view->last_test_id : 0;

		$testCondition = false;
		$testWait = false;
		if($userTest->view->testView->active == 1)
			$testCondition = true;
		else {
			if($userTest->view->testView->waiting == 1) {
				$testCondition = true;
				$testWait = true;
			}
		}

		if($testWait == true)
			$this->redirect(Yii::app()->createUrl('site/index'));

		if(Yii::app()->request->isPostRequest) {
			if(Yii::app()->getRequest()->getParam('reset') && $testCondition) {

			}
			Yii::app()->end();
		}

		$this->pageTitle = Yii::t('phrase', 'Atention');
		$this->pageDescription = '';
		$this->pageMeta = '';
		$this->render('front_index', array(
			'setting'=>$setting,
			'testCondition'=>$testCondition,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionAgree() 
	{
		$user_id = Yii::app()->user->id;
		$setting = AssessmentCareerSetting::model()->findByPk(1);
		$userTest = AssessmentCareerUsers::model()->findByAttributes(array(
			'user_id'=>$user_id,
		));
		$testID = $userTest ? $userTest->view->last_test_id : 0;

		$testCondition = false;
		$testWait = false;
		if($userTest->view->testView->active == 1)
			$testCondition = true;
		else {
			if($userTest->view->testView->waiting == 1) {
				$testCondition = true;
				$testWait = true;
			}
		}

		if($testWait == true)
			$this->redirect(Yii::app()->createUrl('site/index'));

		$this->pageTitle = Yii::t('phrase', 'Agreement');
		$this->pageDescription = '';
		$this->pageMeta = '';
		$this->render('front_agreement', array(
			'setting'=>$setting,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionQuiz() 
	{
		$user_id = Yii::app()->user->id;
		$setting = AssessmentCareerSetting::model()->findByPk(1);
		$userTest = AssessmentCareerUsers::model()->findByAttributes(array(
			'user_id'=>$user_id,
		));
		$testID = $userTest ? $userTest->view->last_test_id : 0;

		$testCondition = false;
		$testWait = false;
		if($userTest->view->testView->active == 1)
			$testCondition = true;
		else {
			if($userTest->view->testView->waiting == 1) {
				$testCondition = true;
				$testWait = true;
			}
		}

		if($testWait)
			$this->redirect(Yii::app()->createUrl('site/index'));

		$criteria=new CDbCriteria;
		$criteria->select = 't.group_id';
		if($testCondition) {
			$criteria->with = array(
				'statements' => array(
					'alias' => 'statements',
				),
				'statements.details' => array(
					'alias' => 'details',
					'on' => 'details.test_id='.$testID,
				),
				'together' => true,
			);
			$criteria->addCondition('details.statement_id IS NULL');
		} else
			$criteria->compare('t.publish', 1);
		$criteria->order = 'RAND()';

		$statementGroup = AssessmentCareerStatementGroup::model()->find($criteria);
		$statementGroupCount = AssessmentCareerStatementGroup::model()->count($criteria);
		if($statementGroup == null && $testID != 0) {
			$model = AssessmentCareerTests::model()->findByPk($testID);
			if(in_array($model->finish_date, array('0000-00-00 00:00:00','1970-01-01 00:00:00','0002-12-02 07:07:12','-0001-11-30 00:00:00'))) {
				$model->finish_date = date('Y-m-d H:i:s');
				if($model->update())
					$this->redirect(Yii::app()->controller->createUrl('result', array('id'=>$model->test_id)));
			}
		}
		
		if(Yii::app()->request->isPostRequest) {
			if(Yii::app()->getRequest()->getParam('start') && !$testCondition) {
				$model=new AssessmentCareerTests;
				$model->user_id = $user_id;
				$model->save();

			} else {
				$model=new AssessmentCareerTestDetail;
				if(isset($_POST['AssessmentCareerTestDetail'])) {
					$model->attributes=$_POST['AssessmentCareerTestDetail'];
					$model->test_id = $testID;

					$jsonError = CActiveForm::validate($model);
					if(strlen($jsonError) > 2) {
						echo $jsonError;
		
					} else {
						if($model->validate()) {
							$statement_id_i = $model->statement_id_i;
							if(is_array($statement_id_i)) {
								foreach($statement_id_i as $key=>$val) {
									AssessmentCareerTestDetail::insertChoice($model->test_id, $key, $val);
								}
							}
						} else
							print_r($model->getErrors());
					}
				}
			}
			Yii::app()->end();
		}

		$this->pageTitle = Yii::t('phrase', 'Tests');
		$this->pageDescription = '';
		$this->pageMeta = '';
		$this->render('front_quiz', array(
			'setting'=>$setting,
			'testCondition'=>$testCondition,
			'statementGroup'=>$statementGroup,
			'statementGroupCount'=>$statementGroupCount,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionResult($id)
	{
		$user_id = Yii::app()->user->id;

		$model = AssessmentCareerTests::model()->findByPk($id);
		if($model->publish != 1 || $model->user_id != $user_id || !$model->view->result)
			$this->redirect(Yii::app()->createUrl('site/index'));

		$interest = AssessmentCareerTests::getInterest($model->result);

		if($model->career_interest == '') {
			$model->career_interest = !empty($interest) ? implode(',', $interest) : 0;
			$model->update();
		}

		$this->pageTitle = Yii::t('phrase', 'Result');
		$this->pageDescription = '';
		$this->pageMeta = '';
		$this->render('front_result', array(
			'model'=>$model,
			'interest'=>$interest,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) 
	{
		$model = AssessmentCareerTests::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404, Yii::t('phrase', 'The requested page does not exist.'));
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) 
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='assessment-career-tests-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
