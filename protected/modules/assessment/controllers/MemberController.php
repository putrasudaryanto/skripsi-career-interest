<?php
/**
 * MemberController
 * @var $this MemberController
 *
 * Reference start
 * TOC :
 *	Index
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 31 July 2018, 19:27 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 *----------------------------------------------------------------------------------------------------------
 */

class MemberController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';
	public $defaultAction = 'index';

	/**
	 * Initialize admin page theme
	 */
	public function init() 
	{
		$arrThemes = $this->currentTemplate('public');
		Yii::app()->theme = $arrThemes['folder'];
		$this->layout = $arrThemes['layout'];
		$this->applyViewPath(__dir__); 
	}

	/**
	 * @return array action filters
	 */
	public function filters() 
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() 
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index'),
				'users'=>array('@'),
				'expression'=>'Yii::app()->user->level',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() 
	{
		$user_id = Yii::app()->user->id;
		$setting = AssessmentCareerSetting::model()->findByPk(1);
		$userInterestTest = AssessmentCareerUsers::model()->findByAttributes(array(
			'user_id'=>$user_id,
		));
		$interestTestID = $userInterestTest ? $userInterestTest->view->last_test_id : 0;

		$interestTestCondition = false;
		$interestTestWait = false;
		if($userInterestTest->view->testView->active == 1)
			$interestTestCondition = true;
		else {
			if($userInterestTest->view->testView->waiting == 1) {
				$interestTestCondition = true;
				$interestTestWait = true;
			}
		}
		$interestTest = array(
			'id'=>$interestTestID,
			'running'=>$interestTestCondition,
			'waiting'=>$interestTestWait,
			'start_date'=>$interestTestID ? $userInterestTest->view->test->start_date : '',
			'next_test_date'=>$interestTestWait ? $userInterestTest->view->test->next_test_date : '',
		);

		$this->pageTitle = Yii::t('phrase', 'Personal Assessments');
		$this->pageDescription = '';
		$this->pageMeta = '';
		$this->render('front_index', array(
			'setting'=>$setting,
			'interestTest'=>$interestTest,
		));
	}

}
